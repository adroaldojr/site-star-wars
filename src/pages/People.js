import { useEffect, useState } from "react";
import Banner from "../components/Banner";
import "bootstrap/dist/css/bootstrap.min.css";
import { api } from "../services/api";

export default function People() {
  const [people, setPeople] = useState([]);

  useEffect(() => {
    api.get("/people").then((response) => {
      setPeople(response.data["results"]);
    });
  }, []);

  return (
    <>
      <Banner titulo="Personagens" mensagem="Descrição" imagemBanner="people" />
      <section id="two" className="wrapper style1 special">
        <table className="table">
          <thead>
            <tr>
              <th scope="col">Nome</th>
              <th scope="col">Altura</th>
              <th scope="col">Peso</th>
              <th scope="col">Cor do Cabelo</th>
              <th scope="col">Cor da Pele</th>
              <th scope="col">Cor dos Olhos</th>
              <th scope="col">Ano Nascimento</th>
              <th scope="col">Genêro</th>
            </tr>
          </thead>
          <tbody>
            {people.map((person, index) => {
              return (
                <tr key={index}>
                  <td>{person.name}</td>
                  <td>{person.height}</td>
                  <td>{person.mass}</td>
                  <td>{person.hair_color}</td>
                  <td>{person.skin_color}</td>
                  <td>{person.eye_color}</td>
                  <td>{person.birth_year}</td>
                  <td>{person.gender}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </section>
    </>
  );
}
