import { useEffect, useState } from "react";
import Banner from "../components/Banner";
import "bootstrap/dist/css/bootstrap.min.css";
import { api } from "../services/api";

export default function Species() {
  const [species, setSpecies] = useState([]);

  useEffect(() => {
    api.get("/species").then((response) => {
      let species = response.data["results"];
      let dados = species.map(async (item) => {
        let planeta = await api.get(item.homeworld);
        return Object.assign({}, item, { planeta: planeta.data });
      });

      Promise.all(dados).then((resu) => {
        console.log(resu);
        setSpecies(resu);
      });
    });
  }, []);

  return (
    <>
      <Banner titulo="Especies" mensagem="Descrição" imagemBanner="species" />
      <section id="two" className="wrapper style1 special">
        <table className="table">
          <thead>
            <tr>
              <th scope="col">Nome</th>
              <th scope="col">Planeta Natal</th>
              <th scope="col">Língua</th>
              <th scope="col">Classificação</th>
              <th scope="col">Altura média</th>
              <th scope="col">Média de vida</th>
              <th scope="col">Designação</th>
            </tr>
          </thead>
          <tbody>
            {species.map((species, index) => {
              return (
                <tr key={index}>
                  <td>{species.name}</td>
                  <td>{species.planeta.name}</td>
                  <td>{species.language}</td>
                  <td>{species.classification}</td>
                  <td>{species.average_height}</td>
                  <td>{species.average_lifespan}</td>
                  <td>{species.designation}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </section>
    </>
  );
}
