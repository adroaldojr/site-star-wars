import { useEffect, useState } from "react";
import Banner from "../components/Banner";
import "bootstrap/dist/css/bootstrap.min.css";
import { api } from "../services/api";

export default function Films() {
  const [films, setFilms] = useState([]);

  useEffect(() => {
    api.get("/films").then((response) => {
      setFilms(response.data["results"]);
    });
  }, []);

  return (
    <>
      <Banner titulo="Filmes" mensagem="Descrição" imagemBanner="films" />
      <section id="two" className="wrapper style1 special">
        <table className="table">
          <thead>
            <tr>
              <th scope="col">Título</th>
              <th scope="col">Diretor</th>
              <th scope="col">Número do episódio</th>
              <th scope="col">Lançamento</th>
            </tr>
          </thead>
          <tbody>
            {films.map((films, index) => {
              return (
                <tr key={index}>
                  <td>{films.title}</td>
                  <td>{films.director}</td>
                  <td>{films.episode_id}</td>
                  <td>{films.release_date}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </section>
    </>
  );
}
