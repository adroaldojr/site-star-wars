import React from "react";
import Banner from "../components/Banner";
import Card from "react-bootstrap/Card";
import luig from "../images/luig.jpg";
import adroaldo from "../images/adroaldo.jpg";
import vinicius from "../images/vinicius.png";

export default function Home() {
  return (
    <>
      <Banner
        titulo="Star Wars"
        mensagem="Um portal de Star Wars onde você achará todas as informações sobre a SAGA"
        imagemBanner="home"
      />
      <section id="three" className="wrapper special">
        <div className="inner">
          <header className="align-center">
            <h2 style={{ fontWeight: 500 }}>Sobre a saga Star Wars</h2>
          </header>
          <Card className="text-center content-card-star">
            <Card.Body>
              <Card.Title className="card-title-star">Star Wars</Card.Title>
              <Card.Text className="text-body-card-star">
                Para alguns é apenas um conjunto de filmes de aventuras, para
                outros é quase uma religião, para outros tantos ainda é um
                imenso ponto de interrogação traduzido num universo
                incompreensível que leva adultos a vestirem-se como crianças e a
                atafulharem a casa de bonecada da saga.{" "}
              </Card.Text>

              <Card.Text className="text-body-card-star">
                Uma coisa é certa: é um verdadeiro fenómeno global e em 2015,
                após dez anos de ausência do grande ecrã, “Star Wars: O
                Despertar da Força” conseguiu reacender a paixão dos adultos que
                já conheciam o universo de trás para a frente e iniciá-la nas
                crianças que nunca tinham viram um único filme da saga. Mas de
                que é que falamos afinal quando falamos de “Star Wars”?
              </Card.Text>

              <Card.Text className="text-body-card-star">
                Se quisermos associar um género aos nove filmes da saga
                principal, talvez escolhêssemos dizer que são verdadeiros épicos
                com elementos de western e de fitas de capa e espada injectados
                num universo de ficção científica.
              </Card.Text>

              <Card.Text className="text-body-card-star">
                Há princesas, pistolas, duelos, dinastias, espadas, cavalgadas
                heróicas, naves espaciais, robôs e criaturas alienígenas, num
                enredo que mistura ciência e feitiçaria, vitórias esfuziantes e
                tragédias épicas, bons que passam a maus e maus que passam a
                bons, e relações de aprendizagem e ruptura entre pais e filhos e
                entre mestres e aprendizes. E claro, é preciso não esquecer a
                influência dos conto de fadas, bem marcada pela inscrição que
                abre cada filme:
              </Card.Text>
            </Card.Body>
          </Card>
        </div>
      </section>

      <section id="two" className="wrapper style1 special">
        <div className="inner">
          <header>
            <h2>Desenvolvedores</h2>
          </header>
          <div className="flex flex-4">
            <div className="box person">
              <div className="image round">
                <img src={adroaldo} alt="" />
              </div>
              <h3>Adroaldo Leão</h3>
              <span>
                <b>RA:</b> 1070035
              </span>
              <br />
              <span>adroaldosouto@gmail.com</span>
            </div>
            <div className="box person">
              <div className="image round">
                <img src={luig} alt="" />
              </div>
              <h3>Luig Léo</h3>
              <span>
                <b>RA:</b> 1118099
              </span>
              <br />
              <span>luigleo@gmail.com</span>
            </div>
            <div className="box person">
              <div className="image round">
                <img src={vinicius} alt="" />
              </div>
              <h3>Vinicius Oliveira</h3>
              <span>
                <b>RA:</b> 1121337
              </span>
              <br />
              <span>vini_oliveira_17@yahoo.com.br</span>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}
