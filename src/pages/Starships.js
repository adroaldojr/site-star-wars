import { useEffect, useState } from "react";
import Banner from "../components/Banner";
import "bootstrap/dist/css/bootstrap.min.css";
import { api } from "../services/api";

export default function Starships() {
  const [starship, setStarship] = useState([]);

  useEffect(() => {
    api.get("/starships").then((response) => {
      setStarship(response.data["results"]);
    });
  }, []);

  return (
    <>
      <Banner
        titulo="Starships"
        mensagem="Descrição"
        imagemBanner="starships"
      />
      <section id="two" className="wrapper style1 special">
        <table className="table">
          <thead>
            <tr>
              <th scope="col">Nome</th>
              <th scope="col">Modelo</th>
              <th scope="col">Ranking Hyperdrive</th>
              <th scope="col">Tripulação</th>
              <th scope="col">Fabricante</th>
            </tr>
          </thead>
          <tbody>
            {starship.map((starship, index) => {
              return (
                <tr key={index}>
                  <td>{starship.name}</td>
                  <td>{starship.model}</td>
                  <td>{starship.hyperdrive_rating}</td>
                  <td>{starship.crew}</td>
                  <td>{starship.manufacturer}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </section>
    </>
  );
}
