import { useEffect, useState } from "react";
import Banner from "../components/Banner";
import "bootstrap/dist/css/bootstrap.min.css";
import { api } from "../services/api";

export default function Planets() {
  const [planets, setPlanets] = useState([]);

  useEffect(() => {
    api.get("/planets").then((response) => {
      setPlanets(response.data["results"]);
    });
  }, []);

  return (
    <>
      <Banner titulo="Planetas" mensagem="Descrição" imagemBanner="planets" />
      <section id="two" className="wrapper style1 special">
        <table className="table">
          <thead>
            <tr>
              <th scope="col">Nome</th>
              <th scope="col">Clima</th>
              <th scope="col">População</th>
              <th scope="col">Terreno</th>
              <th scope="col">Diâmetro</th>
            </tr>
          </thead>
          <tbody>
            {planets.map((planets, index) => {
              return (
                <tr key={index}>
                  <td>{planets.name}</td>
                  <td>{planets.climate}</td>
                  <td>{planets.population}</td>
                  <td>{planets.terrain}</td>
                  <td>{planets.diameter}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </section>
    </>
  );
}
