import React from "react";
import Banner from "../components/Banner";
import "bootstrap/dist/css/bootstrap.min.css";

export default function Login() {
  return (
    <>
      <Banner
        titulo="Login"
        mensagem="Acesse para usufruir dos beneficios!"
        imagemBanner="login"
      />
      <section>
        <div className="container login-container">
          <div className="row">
            <div className="col-md-3"></div>
            <div className="col-md-6 login-form-2">
              <h3>Entre em sua conta</h3>
              <form>
                <div className="form-group">
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Informe seu e-mail"
                  />
                </div>
                <div className="form-group ">
                  <input
                    type="password"
                    className="form-control form-password"
                    placeholder="Informe sua senha"
                  />
                </div>
                <div className="form-group">
                  <input
                    type="submit"
                    className="btnSubmit botao-entrar-conta"
                    value="Entrar"
                  />
                </div>
              </form>
            </div>
            <div className="col-md-3"></div>
          </div>
        </div>
      </section>
    </>
  );
}
