import React from "react";

export default function Footer() {
  return (
    <footer id="footer">
      <div className="inner">
        <div className="flex">
          <div className="copyright">
            &copy; Desenvolvido por:{" "}
            <a href="https://www.instagram.com/jradroaldo/">Adroaldo Souto</a> ,{" "}
            <a
              href="https://www.instagram.com/luigleo/"
              target="_blank"
              rel="noopener noreferrer"
            >
              Luig Léo
            </a>{" "}
            e{" "}
            <a
              href="https://www.instagram.com/_vine.h/"
              target="_blank"
              rel="noopener noreferrer"
            >
              Vinicius Oliveira
            </a>{" "}
            .
          </div>
          <ul className="icons">
            <li>
              <a
                href="https://www.facebook.com/StarWars/"
                target="_blank"
                rel="noopener noreferrer"
                className="icon fa-facebook"
              >
                <span className="label">Facebook</span>
              </a>
            </li>
            <li>
              <a
                href="https://www.instagram.com/starwars/"
                target="_blank"
                rel="noopener noreferrer"
                className="icon fa-instagram"
              >
                <span className="label">Instagram</span>
              </a>
            </li>
            <li>
              <a
                href="https://twitter.com/starwars"
                target="_blank"
                rel="noopener noreferrer"
                className="icon fa-twitter"
              >
                <span className="label">Twitter</span>
              </a>
            </li>
            <li>
              <a
                href="https://www.starwars.com/"
                target="_blank"
                rel="noopener noreferrer"
                className="icon fas fa-link"
              >
                <span className="label">Site</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </footer>
  );
}
