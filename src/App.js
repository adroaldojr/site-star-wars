import React from 'react';
import './assets/css/main.css'
import Header from './components/Header'
import Footer from './components/Footer'
import Home from './pages/Home'
import Films from './pages/Films'
import Starships from './pages/Starships'
import People from './pages/People'
import Planets from './pages/Planets'
import Species from './pages/Species'
import Login from './pages/Login'
import { HashRouter, Switch, Route } from 'react-router-dom'

function App() {
	return (
		<>
			<HashRouter>
				<Header />
				<Switch>
					<Route path="/" exact={true} component={Home}/>
					<Route path="/Films" component={Films}/>
					<Route path="/Starships" component={Starships}/>
					<Route path="/People" component={People}/>
					<Route path="/Planets" component={Planets}/>
					<Route path="/Species" component={Species}/>
					<Route path="/Login" component={Login}/>
				</Switch>
			</HashRouter>
			<Footer />
		</>
	);
}
export default App;
